package com.nearbuy.missingBankIds.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;

import com.nearbuy.missingBankIds.main.MissingBankIds_v1;



public class Functions {
	
	public static int irequestedDays=27;
//	public static String dt = "02-19";
	
	static Calendar currDate = Calendar.getInstance();
	public static  String date = getCurrDate();
	public static String dt = date;
	
	public static String getCurrDate(){
		currDate = Calendar.getInstance();
		String sdate="";
		String sMonth="";
		String sDay="";
		
		int iMonth = currDate.get(Calendar.MONTH)+1;
		if(iMonth < 10) sMonth= ("0"+Integer.toString(iMonth));
		else sMonth= Integer.toString(iMonth);
		
		int iDayofMonth = currDate.get(Calendar.DAY_OF_MONTH);
		if(iDayofMonth < 10) sDay= ("0"+Integer.toString(iDayofMonth));
		else sDay= Integer.toString(iDayofMonth);
		
		sdate = sMonth+"-"+sDay;
		return sdate;
	}
	
	public static void createCalInExcel(Xls_Reader xls) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			System.out.println("Date ::" +date);
			c.setTime(sdf.parse(dt));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < irequestedDays; i++) {
			dt = sdf.format(c.getTime());
			// int day_of_month=c.get(Calendar.DAY_OF_MONTH);
			xls.setCellData("TravelDeals", 5 + i, 1, dt);
			c.add(Calendar.DATE, 1);
		}

	}
	
	public static int getUrlResponseCode(String urlString){
		URL url;
		int responseCode=0;
		try {
			url = new URL(urlString);
		HttpURLConnection http = null;
		http = (HttpURLConnection) url.openConnection();
		http.setRequestMethod("GET");
		Thread.sleep(500);
		http.connect();
		Thread.sleep(500);
		responseCode = http.getResponseCode();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Get Response Code
		return responseCode;
	}

	public static String getMonthinNum(String smonth){
		Calendar c = Calendar.getInstance();
		String monthInNum=Integer.toString(c.get(Calendar.DAY_OF_MONTH))+"-";
		if(smonth.equals("January")) monthInNum="01-";
		if(smonth.equals("February")) monthInNum="02-";
		if(smonth.equals("March")) monthInNum="03-";
		if(smonth.equals("April")) monthInNum="04-";
		if(smonth.equals("May")) monthInNum="05-";
		if(smonth.equals("June")) monthInNum="06-";
		if(smonth.equals("July")) monthInNum="07-";
		if(smonth.equals("August")) monthInNum="08-";
		if(smonth.equals("September")) monthInNum="09-";
		if(smonth.equals("October")) monthInNum="10-";
		if(smonth.equals("November")) monthInNum="11-";
		if(smonth.equals("December")) monthInNum="12-";
		return monthInNum;
	}
	
	public static File createReportFile() throws IOException{
		
			File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") + "Results");
			
			if (!file.exists()){
				file.mkdir();
			}
			
			File dest = new File(System.getProperty("user.dir") + "/Results/MissingBankIdsReport_"+date+".xlsx");
				try{
				InputStream ReportFormat = Functions.class.getClassLoader().getResourceAsStream("MissingBankIdsReport_Format.xlsx");
				
//				Files.deleteIfExists(dest.toPath());
				FileUtils.copyInputStreamToFile(ReportFormat, dest);
				
				}catch(Exception e){
					e.printStackTrace();
				}
		return dest;
		

		
	}
	
	public static void setDealIdsToFile(ArrayList<Object> dealIds) throws Exception {
		
	//	File DealfileName = new File (System.getProperty("user.dir") + "/resources/Files/HulkDeals.txt");
		
		
		// Delete file if Exists
	//	Files.deleteIfExists(new File (System.getProperty("user.dir") + "/src/main/resources/HulkDeals_"+date+".txt").toPath());
		
		// Deal Ids File Path and Create new File
		File dealFile = new File(System.getProperty("user.dir") + "/src/main/resources/HulkDeals_"+date+".txt");
		dealFile.createNewFile();

		FileWriter fw = new FileWriter(dealFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		for(int loop=0;loop < dealIds.size() ; loop++){
			String dealId = Long.toString((long) dealIds.get(loop));
			bw.write(dealId);
			bw.newLine();
		}
		bw.close();
	}

	
}
