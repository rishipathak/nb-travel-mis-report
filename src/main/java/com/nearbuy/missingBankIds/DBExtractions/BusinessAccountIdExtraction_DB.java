package com.nearbuy.missingBankIds.DBExtractions;

import java.util.ArrayList;
import java.util.Calendar;

import com.mongodb.BasicDBObject;
import com.nearbuy.missingBankIds.utils.Functions;
import com.nearbuy.missingBankIds.utils.MongoQuery;
import com.nearbuy.missingBankIds.utils.MongoUtil;
import com.nearbuy.missingBankIds.utils.SshManager;

public class BusinessAccountIdExtraction_DB {

	static MongoUtil mapping_svc = null;
	static String collection = "mapping";
	
	public static void createMappingMongoConnection() throws Exception {
		
		mapping_svc = new MongoUtil();
		
		// Create Prod Mapping Svc Mongo Connection
		SshManager.createSshConnection("10.2.1.143", "27017", "3070");
		mapping_svc.createMongoConnection("nb-mapping","3070");
		
	}
	
	
	public static String getBusinessAccId(String dealId){
		String  businessAccId = "";
		
		// Mongo Query
		BasicDBObject query = new BasicDBObject("type","deal")
							.append("id",dealId);
		
							
		System.out.println("Mapping Svc query :" + query.toString());
		
		// Fire Mongo Query for Getting Business Account Id
		System.out.println("Running Query on Mapping Svc Mongo DB to fetch Business Account Ids");
		businessAccId = MongoQuery.findWithQuerywithNestedObject(mapping_svc.db, collection, query, 10, "mappings","businessAccount","id");
//		System.out.println("Query Ran successfully \n");
		System.out.println("Business Account Id for Deal Id:" + dealId + " is " + businessAccId);
		
		return businessAccId;
		
	}
	
	public static void closeConnections(){	
		//close db and ssh connections
		mapping_svc.closeConnectionMongo();
		SshManager.closeConnection();
		System.out.println("Database Connection With Mapping Service Closed");
	}
	
	
//	public static void main(String[] args) throws Exception
//	{
//		BusinessAccountExtraction_DB.createMappingMongoConnection();
//		BusinessAccountExtraction_DB.getBusinessAccId("3019902");
//		BusinessAccountExtraction_DB.closeConnections();
//	}
}
