package com.nearbuy.missingBankIds.utils;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * 
 * @author puneet
 *
 */
public class MongoUtil {

	MongoClient mongoClient=null;
	public DB db=null;
	
	/**
	 * 
	 * @param mongoDb
	 * @param localPort
	 * @throws Exception
	 */
	public void createMongoConnection(String mongoDb,String localPort) {

		try {
			mongoClient = new MongoClient("127.0.0.1", Integer.parseInt(localPort));
			this.db = mongoClient.getDB(mongoDb);
			System.out.println("MongoDB connection created successfully !!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeConnectionMongo() {
		this.mongoClient.close();
		System.out.println("MongoDB connection closed successfully !!!");
	}
	
	
}
