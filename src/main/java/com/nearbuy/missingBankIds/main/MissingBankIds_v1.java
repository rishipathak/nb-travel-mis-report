package com.nearbuy.missingBankIds.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.nearbuy.missingBankIds.DBExtractions.BankIdExtraction_DB;
import com.nearbuy.missingBankIds.DBExtractions.BusinessAccountIdExtraction_DB;
import com.nearbuy.missingBankIds.DBExtractions.DealsExtraction_DB;
import com.nearbuy.missingBankIds.utils.EmailUtil;
import com.nearbuy.missingBankIds.utils.Functions;
import com.nearbuy.missingBankIds.utils.SshManager;
import com.nearbuy.missingBankIds.utils.Xls_Reader;

public class MissingBankIds_v1 {

	static FileInputStream fis;
	static int count = 2;

	public static void main(String[] args) throws Exception {
		
		File reportFile = Functions.createReportFile();
		Xls_Reader xls_reader = new Xls_Reader(reportFile.getPath());

		DealsExtraction_DB.getDealIds();
		
		BusinessAccountIdExtraction_DB.createMappingMongoConnection();
		BankIdExtraction_DB.createAccountPostgreConnection();
		
		// Capture Start Time
		Calendar cal = Calendar.getInstance();
		String start_time = cal.getTime().toLocaleString();
		System.out.println("start_time : " + start_time);

		// File with Deal Ids (Missing Bank Account Ids)
		
		String DealfileName = System.getProperty("user.dir") + "/src/main/resources/HulkDeals_"+Functions.date+".txt";
		
		// Read dealId from file
		try (BufferedReader br = new BufferedReader(new FileReader(new File(DealfileName)))) {
			String dealId ="";
			

			// Read dealId from file
			while ((dealId = br.readLine()) != null) {
				
				String dealOwner="";
				String businessAccId ="";
				String bankAccId="";
				String decisionMakerId="";
				String decisionMakerEmail="";
				String decisionMakerName="";
				String decisionMakerPhone="";
				String accountLegalName="";
				String accountName="";
				String category="";

				try {
					xls_reader.setCellData("Bank Ids Missing Deals", "Hulk Deal Id", count, dealId);
					System.out.println(count + ".  dealId :: " + dealId);
					
					// Get Business Account Id from Mapping Service
					businessAccId = BusinessAccountIdExtraction_DB.getBusinessAccId(dealId);
					xls_reader.setCellData("Bank Ids Missing Deals", "Business Account Id (Mapping Service)", count, businessAccId);
					
					// Get Bank Account Ids from Account Service
					if(!businessAccId.equals("")){
						bankAccId = BankIdExtraction_DB.getBankAccIds(businessAccId, BankIdExtraction_DB.account_svc.connection);
						xls_reader.setCellData("Bank Ids Missing Deals", "Bank Ids (Account Service)", count, bankAccId.replace("[","").replace("]",""));
					}
					
					// Get Decision Maker Id for DealId from Deal Service
					decisionMakerId=DealsExtraction_DB.getDecisionMakerId(dealId);
					if(!decisionMakerId.equals(""))
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Id", count, decisionMakerId);
					else
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Id", count, "Decision Maker Id not Present");
					
					// Get Category Id from Deal Service
					category = DealsExtraction_DB.getCategoryId(dealId);
					if(!category.equals(""))
						xls_reader.setCellData("Bank Ids Missing Deals", "Category", count, category);
					else
						xls_reader.setCellData("Bank Ids Missing Deals", "Category", count, "Category Id not Present");
					
					// Get Decision Maker Email from Account Service
					decisionMakerEmail=BankIdExtraction_DB.getDecisionMakerEmailId(decisionMakerId,BankIdExtraction_DB.account_svc.connection).split(",")[0];
					if(!decisionMakerEmail.equals(""))
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Email", count, decisionMakerEmail.replace("[","").replace("]",""));
					else
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Email", count, "Active Decision Maker Not Present");
					
					// Get Decision Maker Name from Account Service
					decisionMakerName=BankIdExtraction_DB.getDecisionMakerEmailId(decisionMakerId,BankIdExtraction_DB.account_svc.connection).split(",")[1];
					if(decisionMakerName.equals("") ||  decisionMakerName.contains("null")){
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Name", count, "Active Decision Maker Name Not Present");
					}else{
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Name", count, decisionMakerName.replace("[","").replace("]",""));
					}
						
					
					// Get Decision Maker Phone from Account Service
					decisionMakerPhone = BankIdExtraction_DB.getPhoneNumbers(decisionMakerId, BankIdExtraction_DB.account_svc.connection);
					if(!decisionMakerPhone.equals(""))
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Phone", count, decisionMakerPhone);
					else
						xls_reader.setCellData("Bank Ids Missing Deals", "Decision Maker Phone", count, "Active Decision Maker Phone Not Present");
					
					
					// Get Deal Owner from  Deal Service
					dealOwner =DealsExtraction_DB.getDealOwner(dealId);
					xls_reader.setCellData("Bank Ids Missing Deals", "Deal Owner", count, dealOwner.replace("[","").replace("]",""));
					
					// Get Name and Legal name from Account Service (for Business Account Ids)
					accountName = BankIdExtraction_DB.getNameAndLegalName(businessAccId, BankIdExtraction_DB.account_svc.connection).split(",")[0];
					if(!accountName.equals(""))
						xls_reader.setCellData("Bank Ids Missing Deals", "Account Name", count, accountName);
					else
						xls_reader.setCellData("Bank Ids Missing Deals", "Account Name", count, "Active Bank Acc Id Name Not Present");
					
					accountLegalName = BankIdExtraction_DB.getNameAndLegalName(businessAccId, BankIdExtraction_DB.account_svc.connection).split(",")[1];
					if(!accountLegalName.equals(""))
						xls_reader.setCellData("Bank Ids Missing Deals", "Account Legal Name", count, accountLegalName);
					else
						xls_reader.setCellData("Bank Ids Missing Deals", "Account Legal Name", count, "Active Bank Acc Id Legal Name Not Present");
					
					
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Exception Occured ::" + dealId);
				}
				count++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		BusinessAccountIdExtraction_DB.closeConnections();;
		BankIdExtraction_DB.closeConnections();
		DealsExtraction_DB.closeConnections();
		
		// send Email Report
//		EmailUtil.sendEmail(reportFile.getPath(), "rishi.pathak@nearbuy.com,snehesh.mitra@nearbuy.com,mahesh.sharma@nearbuy.com,"
//				+ "sunny.sharma@nearbuy.com,anuj.sharma@nearbuy.com,sweta.choudhary@nearbuy.com,"
//				+ "abhishek.bhagia@nearbuy.com,priyanka.sarkar@nearbuy.com,alekh.agarwal@nearbuy.com",
//				"nearbuy11@gmail.com", "nbtesting321",
//				"Deals with Missing Bank Account Ids Report :: " + Functions.date, "Hi, <br> </br> <br> </br> Please see attached Report for Live Deals with Missing Bank Account Ids."
//						+ "<br> </br> <br> </br> ** This is an Auto Generated Email."
//						+ "<br> </br> <br> </br> Thanks! <br> </br> <br> </br> Regards, "
//						+ "<br> </br> ~Rishi");
		
		EmailUtil.sendEmail(reportFile.getPath(), "rishi.pathak@nearbuy.com",
				"nearbuy11@gmail.com", "nbtesting321",
				"Deals with Missing Bank Account Ids Report :: " + Functions.date, "Hi, <br> </br> <br> </br> Please see attached Report for Live Deals with Missing Bank Account Ids."
						+ "<br> </br> <br> </br> ** This is an Auto Generated Email."
						+ "<br> </br> <br> </br> Thanks! <br> </br> <br> </br> Regards, "
						+ "<br> </br> ~Rishi");

		Calendar cal1 = Calendar.getInstance();
		String end_time = cal1.getTime().toLocaleString();
		System.out.println("End_time : " + end_time);

	}
}
