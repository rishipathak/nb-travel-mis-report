package com.nearbuy.missingBankIds.DBExtractions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import com.mongodb.BasicDBObject;
import com.nearbuy.missingBankIds.utils.Functions;
import com.nearbuy.missingBankIds.utils.MongoQuery;
import com.nearbuy.missingBankIds.utils.MongoUtil;
import com.nearbuy.missingBankIds.utils.PostgreUtil;
import com.nearbuy.missingBankIds.utils.SshManager;

public class BankIdExtraction_DB {

	public static PostgreUtil account_svc;
	
	public static void createAccountPostgreConnection() throws Exception {
		
		account_svc = new PostgreUtil();
		
		// Create Prod Account Service Postgres Connection
		SshManager.createSshConnection("nb-prod-merchantuser-db.c6vqep7kcqpl.ap-southeast-1.rds.amazonaws.com", "5432", "3071");
		account_svc.postgreConnection("3071", "merusepro", "skywa1k3r", "merchantuserprofile");	
	}
	
	
	public static String getBankAccIds(String businessAccId,Connection conn){
		Statement stmt=null;
		ResultSet rs=null;
		ArrayList<String> bankAccId = new ArrayList<>();
		
		// Query for Bank Id
		String query = "select bk.id as bank_id from bank_accounts bk "
				+ "inner join business_accounts ba on ba.baid = bk.businessaccountid where "
				+ "ba.isdeleted = false and bk.isdeleted = false and bk.businessaccountid = "+businessAccId+";";
		
		System.out.println(query);
		try {
			stmt = account_svc.connection.createStatement();
			rs= stmt.executeQuery(query);
			
			while (rs.next()) {
				bankAccId.add(rs.getString("bank_id"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Bank Account Ids for Business Account:"+businessAccId + " is " +bankAccId);
		return bankAccId.toString();
		
	}
	
	public static String getDecisionMakerEmailId(String userId,Connection conn){
		Statement stmt=null;
		ResultSet rs=null;
		ArrayList<String> email = new ArrayList<>();
		
		// Query for Bank Id
		String query = "select emailaddress,username from users "
				+ "where isdeleted=false and userid = "+userId+";";
		
	//	System.out.println(query);
		try {
			stmt = account_svc.connection.createStatement();
			rs= stmt.executeQuery(query);
			
			while (rs.next()) {
				email.add(rs.getString("emailaddress"));
				email.add(rs.getString("username"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String result = email.toString().replace("[","").replace("]","");
		System.out.println("Email Address and Name for Decision Maker Id:"+userId + " is " +result);
		return result;	
	}
	
	public static String getNameAndLegalName(String businessAccId,Connection conn){
		Statement stmt=null;
		ResultSet rs=null;
		ArrayList<String> details = new ArrayList<>();
		
		// Query for Bank Id
		String query = "select name,legalname from business_accounts "
				+ "where isdeleted=false and baid = "+businessAccId+";";
		
		System.out.println(query);
		try {
			stmt = account_svc.connection.createStatement();
			rs= stmt.executeQuery(query);
			
			while (rs.next()) {
				details.add(rs.getString("name"));
				details.add(rs.getString("legalname"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String result = details.toString().replace("[","").replace("]","");
	//	System.out.println("Email Address and Name for Decision Maker Id:"+businessAccId + " is " +result);
		return result;
	}
	
	
	public static String getPhoneNumbers(String userId,Connection conn){
		Statement stmt=null;
		ResultSet rs=null;
		ArrayList<String> phoneNumbers = new ArrayList<>();
		
		// Query for Bank Id
		String query = "select phonenumber from user_phone_numbers "
				+ "where userid = "+userId+";";;
		
		System.out.println(query);
		try {
			stmt = account_svc.connection.createStatement();
			rs= stmt.executeQuery(query);
			
			while (rs.next()) {
				phoneNumbers.add(rs.getString("phonenumber"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String SphoneNumbers = phoneNumbers.toString().replace("[","").replace("]","");
		System.out.println("Phone Number for User Id:"+userId + " are " + SphoneNumbers);
		return SphoneNumbers;
		
	}
	
	
	public static void closeConnections(){	
		//close db and ssh connections
		account_svc.closePostgreConnection();
		SshManager.closeConnection();
		System.out.println("Database Connection for Account Services Closed");
	}
	
	
	public static void main(String[] args) throws Exception
	{
		BankIdExtraction_DB.createAccountPostgreConnection();
		BankIdExtraction_DB.getBankAccIds("1011855",account_svc.connection);
		BankIdExtraction_DB.getDecisionMakerEmailId("1019450",account_svc.connection);
		BankIdExtraction_DB.closeConnections();
	}
}
