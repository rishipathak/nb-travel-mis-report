package com.nearbuy.missingBankIds.utils;

import java.util.ArrayList;

import javax.swing.plaf.synth.SynthSpinnerUI;

import org.bson.BasicBSONObject;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

public class MongoQuery {

	static DBCollection collection;

	public static ArrayList<Object> findWithQuery(DB db,String collectionName, BasicDBObject query,int limit,String Key){

		ArrayList<Object> results = new ArrayList<Object>();
		collection = db.getCollection(collectionName);
		DBCursor cursor = collection.find(query).limit(limit);

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				results.add(obj.get(Key));
			}

		} finally {
			cursor.close();
		}

		return results;

	} 
	
	// Specific to Mapping Service
	public static String findWithQuerywithNestedObject(DB db,String collectionName, BasicDBObject query,int limit,String key1, String key2,String key3){

		String result = "";
		collection = db.getCollection(collectionName);
		DBCursor cursor = collection.find(query).limit(limit);

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				DBObject value1 = (DBObject) obj.get(key1);
				DBObject value2 = (DBObject) ((BasicDBList) value1.get(key2)).get(0);
				String value3 = (String) value2.get(key3);
				result = value3;
			}

		} finally {
			cursor.close();
		}

		return result;

	} 
	
	// Specific to Finding Decision Maker Id
	public static String findWithQuerywithTripleNesting(DB db,String collectionName, BasicDBObject query,int limit,String key1, String key2,String key3){

		String result = "";
		collection = db.getCollection(collectionName);
		DBCursor cursor = collection.find(query).limit(limit);

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				DBObject value1 = (DBObject) obj.get(key1);
				DBObject value2 = (DBObject) ((DBObject) value1.get(key2));
				Object value3 = value2.get(key3);
				result = value3.toString().replace(".0","" );
			}

		} finally {
			cursor.close();
		}

		return result;

	} 
	

	public static ArrayList<Object> findWithQuery(DB db,String collectionName, BasicDBObject query,BasicDBObject field,BasicDBObject sortField) throws Exception{


		ArrayList<Object> results = new ArrayList<Object>();
		collection = db.getCollection(collectionName);
		
		System.out.println("collection -"+collection.getFullName());
//		System.out.println("collect -"+collection.getName());
//		System.out.println("findOne -"+collection.findOne().toString());
//		System.out.println("Count all-"+collection.count());
//		System.out.println("Count -"+collection.find().count());
//		System.out.println("Count -"+collection.find(query).count());
		
		DBCursor cursor = null;
		if (field != null){
			if (sortField != null)
				cursor = collection.find(query,field).sort(sortField);
			else
				cursor = collection.find(query,field);
		}
		else 
			cursor = collection.find(query);
		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				//results.add(obj);
				if (field != null){
					for (String keys : obj.keySet()) {
						//results.add(obj.get(keys));
						System.out.println("Keys -"+keys);
					}
					results.add(obj);
				}else
					results.add(obj);
			}

		} 
		finally {
			cursor.close();
		}

		return results;

	} 


	public static int removeManyWithQuery(DB db,String collectionName,BasicDBObject query){

		if(query == null){
			System.out.println("Removing all rows not supported");
			return 0;
		}
//		System.out.println(query.toString());
		collection = db.getCollection(collectionName);
		WriteResult c1 = collection.remove(query);

		return c1.getN();

	}
	
	public static int removeSpecificField(DB db,String collectionName,BasicDBObject query,BasicDBObject update){
		
		if(query == null){
			System.out.println("Removing all rows not supported");
			return 0;
		}
		collection = db.getCollection(collectionName);
		WriteResult updateField = collection.update(query, update);
		return updateField.getN();
		
	}

}
