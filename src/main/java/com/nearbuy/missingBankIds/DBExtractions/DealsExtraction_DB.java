package com.nearbuy.missingBankIds.DBExtractions;

import java.util.ArrayList;
import java.util.Calendar;

import com.mongodb.BasicDBObject;
import com.nearbuy.missingBankIds.utils.Functions;
import com.nearbuy.missingBankIds.utils.MongoQuery;
import com.nearbuy.missingBankIds.utils.MongoUtil;
import com.nearbuy.missingBankIds.utils.SshManager;

public class DealsExtraction_DB {

	static MongoUtil deal_pf = null;
	static String collection = "deal";
	
	public static void getDealIds() throws Exception {
		
		Calendar cal = Calendar.getInstance();
		System.out.println("Time in Millis : " + cal.getTimeInMillis());
		
		deal_pf = new MongoUtil();
		
		// Create Prod Deal Mongo Connection
		SshManager.createSshConnection("10.2.3.164", "27017", "3069");
		deal_pf.createMongoConnection("dealplatform","3069");
		// Connection Created
		
		
		ArrayList<Object> dealIds = new ArrayList<>();
		
		// Mongo Query for Fetching Deal Ids with Missing Bank Ids
		BasicDBObject query = new BasicDBObject("state","live")
							.append("units.dval.dToDt",new BasicDBObject("$gt",cal.getTimeInMillis()))
							.append("units.dval.dFrmDt",new BasicDBObject("$lt",cal.getTimeInMillis()))
							.append("isActive",true)
							.append("offers.isActive",true)
							.append("units.bi", null);
							
		System.out.println("query :" + query.toString());
		
		// Fire Mongo Query
		System.out.println("Running Query on Deal Platform to fetch Live Deal Ids");
		dealIds = MongoQuery.findWithQuery(deal_pf.db, collection, query, 0, "_id");
		System.out.println("Query Ran successfully \n");
		System.out.println("No. of Live Hulk Deals with missing Bank Ids::" + dealIds.size());
		System.out.println(dealIds);
		
		// Set Deal Ids to a File
		Functions.setDealIdsToFile(dealIds);
		
	}
	
	
	public static String getDealOwner(String dealId){
		ArrayList<Object>  dealOwner = new ArrayList<>();
		
		// Mongo Query
		BasicDBObject query = new BasicDBObject("_id",Long.parseLong(dealId));
//		System.out.println("Deal Owner Find Query :" + query);
							
		// Fire Mongo Query for Getting Deal Owner for Deal
		dealOwner = MongoQuery.findWithQuery(deal_pf.db, collection, query,5, "dealOwner");
//		System.out.println("Query Ran successfully \n");
		System.out.println("Deal Owner for Deal Id:" + dealId + " is " + dealOwner);
		
		return dealOwner.toString();
		
	}
	
	
	public static String getDecisionMakerId(String dealId){
		String  DecsionMaker = "";
		
		// Mongo Query
		BasicDBObject query = new BasicDBObject("_id",Long.parseLong(dealId));
		System.out.println("Decision Maker Find Query :" + query);
							
		// Fire Mongo Query for Getting Deal Owner for Deal
		DecsionMaker = MongoQuery.findWithQuerywithTripleNesting(deal_pf.db, collection, query,10, "units","au","decisionMaker");
//		System.out.println("Query Ran successfully \n");
		System.out.println("Decision Maker Id for Deal Id:" + dealId + " is " + DecsionMaker);
		return DecsionMaker.toString();
	}
	
	public static String getCategoryId(String dealId) {
		ArrayList<Object> CategoryId = new ArrayList<>();
		BasicDBObject query = new BasicDBObject("_id",Long.parseLong(dealId));
		System.out.println("Category Find Query :" + query);
		CategoryId = MongoQuery.findWithQuery(deal_pf.db, collection, query, 10, "dCat");
		String catId = CategoryId.toString().split(":")[1].replace("\"","").replace("}","").replace("]","");
		return catId;
	}
	
	public static void closeConnections(){	
		//close db and ssh connections
		deal_pf.closeConnectionMongo();
		SshManager.closeConnection();
		System.out.println("Database Connection With Deal Platform Closed");
	}
	
//	public static void main(String[] args) throws Exception
//	{
//		DealsExtraction_DB.getDealIds();
//		getDecisionMakerId("3006596");
//		getCategoryId("3006596");
//		DealsExtraction_DB.closeConnections();
//		
//	}
}
