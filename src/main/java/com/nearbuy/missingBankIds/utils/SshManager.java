package com.nearbuy.missingBankIds.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.testng.Assert;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.LocalPortForwarder;
public class SshManager {

	private static Connection connection = null;
	private static List<LocalPortForwarder> lpf = null;
	
	
	/**
	 * This function is used to create a tunnel from bastion to database server
	 * @param mongoIp : Ip of the database server.
	 * @param mongoPort : Port of the mongo/sql/postgre server to be read from the environment file.
	 * @param localPort : Local port for the bastion server from which the tunneling will be done.To be read from the environment file  
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	public static void createSshConnection(String mongoIp,String mongoPort,String localPort) throws Exception {
		try {
			
		//	Prod Bastian
			createSession("54.251.156.18","ubuntu", mongoIp, Integer.parseInt(mongoPort),localPort);
			
//	    //	QA Bastian
//			createSession("54.254.145.178","ubuntu", mongoIp, Integer.parseInt(mongoPort),localPort);
			
			 System.out.println("SSH connection created successfully !!!");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private static void createSession(String sship,String user,String dbip,int forwardPort,String localPort) throws Exception
	{
		SshManager manager = new SshManager();
		lpf = new ArrayList<LocalPortForwarder>();
		manager.ssh(sship,user);
		manager.forwardLocalPort(Integer.parseInt(localPort),dbip,forwardPort);
	}

	private void forwardLocalPort(Integer localport, String remotehost,Integer remoteport) throws InterruptedException {
		try {
			LocalPortForwarder temp = connection.createLocalPortForwarder(localport, remotehost, remoteport);
			lpf.add(temp);
		} catch (IOException e) {
			//e.printStackTrace();

		}
		Thread.sleep(5000);
	}

	
	private void ssh(String sship,String sshusername) throws Exception {
		try {
			connection = new Connection(sship, 22);
			connection.connect();
			File key = null;
			
		//	Prod Pem File
		//	String path = System.getProperty("user.dir")+ "/src/main/resources/nearbuy-parallel-prod-bastion.pem";
			
			InputStream bastianInputStream = SshManager.class.getClassLoader().getResourceAsStream("nearbuy-parallel-prod-bastion.pem");
			String pemFilePath = (System.getProperty("user.dir")+ "/src/main/resources/prod-bastion.pem");
			
			FileUtils.copyInputStreamToFile(bastianInputStream, new File(pemFilePath));
				
//		//	QA Pem File
//			String path = System.getProperty("user.dir")+ "/src/main/resources/nearbuy-qa-public_29092015.pem";
			
			
			System.out.println("In ::" + new File(pemFilePath).getAbsolutePath());
			File f = new File(pemFilePath);
			System.out.println("File "   +f.exists());
			if (f.exists()){
				key = new File(pemFilePath);
			}
//			else{
//				key = new File(System.getProperty("user.dir")+ FileUtil.separator+FileUtil.getConstantValue("ProjectName")+FileUtil.separator+FileUtil.getConstantValue("PrivateKey"));
//			}
			boolean isAuthenticated = connection.authenticateWithPublicKey(
					sshusername, key, null);
			
			
			
			if (isAuthenticated == false) {
				throw new IOException("Authentication failed.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void closeConnection() {
		if (connection != null)
			connection.close();
		System.out.println("SSH connection closed !!!");

	}
}
